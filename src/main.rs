use crate::error::AppError;
use rusty_classifier::{SplitTokenizer, TextClassifier, TextClassifierBuilder};
use serde::{Deserialize, Serialize};
use std::convert::Infallible;
use std::env;
use std::sync::{Arc, RwLock};
use warp::{http::status::StatusCode, reject, reply, Filter, Rejection, Reply};

#[macro_use]
extern crate log;

mod error;

#[tokio::main]
async fn main() {
    env_logger::init();
    let csv_file = env::var("TRAINING_DATA_CSV").expect("A path to a csv file is required");

    let classifier = TextClassifierBuilder::new()
        .tokenizer(Box::new(SplitTokenizer))
        .csv_file_path(&csv_file)
        .build()
        .unwrap();

    let classifier = Arc::new(RwLock::new(classifier));

    let with_classifier = warp::any().map(move || Arc::clone(&classifier));

    let classify = warp::get()
        .and(warp::path("classify"))
        .and(warp::query())
        .and(with_classifier.clone())
        .and_then(classify_handler);

    let reload = warp::post()
        .and(warp::path("reload"))
        .and(with_classifier)
        .and_then(reload_handler);

    let routes = classify
        .or(reload)
        .with(warp::cors().allow_any_origin())
        .recover(handle_rejection);

    warp::serve(routes).run(([127, 0, 0, 1], 3030)).await;
}

#[derive(Deserialize)]
struct ClassifyQuery {
    text: String,
}

#[derive(Serialize)]
struct ClassifyResponse {
    category: String,
}

async fn classify_handler(
    query: ClassifyQuery,
    classifier: Arc<RwLock<TextClassifier>>,
) -> Result<impl Reply, Rejection> {
    let category = try_classify(classifier, &query.text).map_err(|e| reject::custom(e))?;

    debug!(
        "The text: '{}' has been classified as \"{}\"",
        query.text, category
    );

    Ok(reply::with_status(
        reply::json(&ClassifyResponse { category }),
        StatusCode::OK,
    ))
}

fn try_classify(classifier: Arc<RwLock<TextClassifier>>, text: &str) -> Result<String, AppError> {
    let classifier = classifier.read().unwrap();

    match classifier.classify(text) {
        Some(res) => Ok(res),
        None => Err(AppError::ClassifyError(text.to_owned())),
    }
}

async fn reload_handler(classifier: Arc<RwLock<TextClassifier>>) -> Result<impl Reply, Rejection> {
    let csv_file = env::var("TRAINING_DATA_CSV").unwrap();

    tokio::task::spawn_blocking(move || {
        let new_classifier = TextClassifierBuilder::new()
        .tokenizer(Box::new(SplitTokenizer))
        .csv_file_path(&csv_file)
        .build()
        .unwrap();

        let mut classifier = classifier.write().unwrap();
        *classifier = new_classifier;
    });

    Ok(warp::reply())
}

#[derive(Serialize)]
struct ErrorResponse {
    message: String,
}

async fn handle_rejection(err: Rejection) -> Result<impl Reply, Infallible> {
    let code;
    let message;

    if err.is_not_found() {
        code = StatusCode::NOT_FOUND;
        message = "Not Found".to_owned();
    } else if let Some(_) = err.find::<warp::filters::body::BodyDeserializeError>() {
        code = StatusCode::BAD_REQUEST;
        message = "Invalid Body".to_owned();
    } else if let Some(e) = err.find::<AppError>() {
        match e {
            AppError::ClassifyError(text) => {
                error!("The following text could not be classified: {}", text);
                code = StatusCode::BAD_REQUEST;
                message = format!("Could not classify the text: {}", text);
            }
            _ => {
                error!("unhandled application error: {:?}", err);
                code = StatusCode::INTERNAL_SERVER_ERROR;
                message = "Internal Server Error".to_owned();
            }
        }
    } else if let Some(_) = err.find::<warp::reject::MethodNotAllowed>() {
        code = StatusCode::METHOD_NOT_ALLOWED;
        message = "Method Not Allowed".to_owned();
    } else {
        error!("unhandled application error: {:?}", err);
        code = StatusCode::INTERNAL_SERVER_ERROR;
        message = "Internal Server Error".to_owned();
    }

    let json = warp::reply::json(&ErrorResponse { message: message });

    Ok(warp::reply::with_status(json, code))
}
