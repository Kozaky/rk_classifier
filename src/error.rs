#[derive(Debug)]
pub enum AppError {
    ClassifyError(String),
}

impl warp::reject::Reject for AppError {}
