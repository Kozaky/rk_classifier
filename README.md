Configure the next variables in the environment:

- AF_PATH=/opt/arrayfire 
- DYLD_LIBRARY_PATH=$AF_PATH/lib

Run as: 

"""
TRAINING_DATA_CSV=~/Documents/Projects/rk_classifier/training_data.csv RUST_LOG=info cargo run --release
"""

It will initialize a server at 127.0.0.1:3030

Endpoints: 

- GET       /classify?text=${text}
- POST      /reload
